# Another Caffeinated Day Commit Controls

## Conventions

    The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL
    NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED",
    "MAY", and "OPTIONAL" in this document are to be interpreted as
    described in [BCP 14](https://www.rfc-editor.org/bcp/bcp14 [RFC2119](https://www.rfc-editor.org/rfc/rfc2119) 
    [RFC8174](https://www.rfc-editor.org/rfc/rfc8174) when, and only when, they
    appear in all capitals, as shown here.

## Background

Coupling Checkstyle with `pre-commit` Git hooks is not necessarily the most documented exercise in Javaland. 

A Google search often references a [Stackoverflow post](https://stackoverflow.com/questions/50741967/add-checkstyle-as-pre-commit-git-hook)
and a 14 year old! [GitHub gist](https://gist.github.com/davetron5000/37350) by user [davetron5000](https://gist.github.com/davetron5000).

Dave's PERL implementation works; with an edit or two as recommended in the post comments, but it MAY be showing its 
age.

An earlier—and unreleased—implementation of this package leveraged Dave's script. This implementation required some 
"brute force" facilitation, especially to reduce configuration friction in a developer workflow: platform scripts for
Wintel and *nix/BSD/macOS had to be written to configure Git's `core.hookspath`, `maven-exec-plugin` configurations and 
accompanying Maven profiles were tested and documented, and libraries had to be bundled for execution of the 
`pre-commit` hook.

All of this would be assembled in a zip file using Maven's Assembly Plugin for distribution and installation. 

It was all good fun.

As a "one-off" implementation for a single developer or a small team, the install could be tolerable. For an enterprise,
especially one seeking less "institutional knowledge," the davetron5000 `pre-commit` hook MAY have been unsustainable.

Enter native Maven classpath features and Husky.

Husky removes the implemention overhead of custom scripts for setting Git hooks as required by davetron5000's script. In
coupling Husky and Maven with the [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin) by 
[Eirik Sletteberg](https://github.com/eirslett) a far simpler package is possible. How? The `maven-exec-plugin` uses the
project's dependency classpath, natively, to support Checkstyle in the `pre-commit` hook.

And one gets [commitlint](https://commitlint.js.org) mostly for free. Mostly.

# Download

[Maven Central](https://repo1.maven.org/maven2/com/anothercaffeinatedday/commit-controls/)

[Source Code](https://gitlab.com/another15y/commit-controls)

[Demo Project](https://gitlab.com/another15y/commit-controls-demo)

[Install](#maven-pre-commit-for-checkstyle-setup)

[Advanced Usage](#advanced-usage)

[Installation for non-Java Project](#installation-for-non-java-project)

# TL;DR

## Husky

Husky is billed as "Modern, native githooks made easy." Commit Controls aims to pre-package a `commit-msg` and 
`pre-commit` hook to ease Javaland integration of these controls.

Husky requires Node and NPM to run, so this makes for an interesting mix in a Java environment. To faciliate the basics 
of commitlint and Checkstyle in the `commit-msg` and `pre-commit` githooks, developers "install" Commit Controls 
and run a Maven profile, at least once, to "configure" Husky, i.e., set Git's `core.hooksPath` configuration.

### Husky "install"

`hookspath` is a Git configuration providing portability of customized Git hooks. Recall, the `.git/hooks` directory is 
not committed to a project's repository and not portable across workstations. Sure, a team lead or team member could 
"onboard" new members. This "institutional knowledge" is easily lost and can be hard on maintainability.

`mvn -Phusky install` runs the necessary features via NPM to point the Git configuration to Husky's Git hooks in the 
target project.

Each developer MUST run this command once to activate the commitlint `commit-msg` and Checkstyle `pre-commit` hooks, but
the project is otherwise ready for use on `clone`.

#### Developers sometimes need a nudge

READMEs for projects SHOULD include specific notes to run the `husky` profile as shown above. However, engineering leads
and merge performers may occasionally find commits on Merge Requests—_Pull Requests_ in GitHub—that clearly were not 
screened by commitlint or Checkstyle.

Missing commitlint commit messages are easily corrected in the merge request by squashing the commit and editing the 
message. However, engineering leads SHOULD reach out to the committer and assist with performing the task and aligning 
with the project expectations, especially where Checkstyle is in place.

### Included Husky Configuration

This package, as noted, has Husky `commit-msg` and `pre-commit` hooks defined.

This configuration is not unlike the following shell log:

```bash
# creates a package.json
npm install husky --save-dev
npm install @commitlint/cli --save-dev
npm install @commitlint/config-conventional --save-dev
npm install commitlint-plugin-function-rules --save-dev
npm install is-ci
# creates .husky (and sets core.hookspath locally)
npx husky install
# creates commit-msg hook
npx husky add .husky/commit-msg 'npx --no -- commitlint --edit "\${1}"'
# HEREDOC creating commitlint config
cat << EOF >> commitlint.config.js
module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    scope-enum: [1, 'always', []]
  },
};
EOF
```

## Maven <span style="font-family: monospace">pre-commit</span> for Checkstyle Setup

### 0. Extract the release zip

Run `unzip commit-controls-1.0.7 -d /path/to/project/root` to deposit the files on the target project.

:warning: It is possible to extract the release to the Finder (macOS), Windows Explorer, or Linux File Manager and drag 
the files to the target project. Note, *nix/macOS "dot-files," e.g., `.husky`, are "invisible." It MAY be necessary to 
view "dot-files" to fully copy the files to the target project. "Shift-Option-." will allow macOS users to see these 
files if not already displayed. YMMV on Linux.

#### Non-Java Project?

Wait! Can I use this to kickstart a non-Java project? Yes! [Skip to the end.](#installation-for-non-java-project)

### 1. Add project properties

It is a good practice to parameterize project dependency and plugin versions. commit-controls assumes that the target 
project POM follows this practice.

```xml
<project>
  ...
  <properties>
    <!-- other properties MAY be present                              -->
    <frontend-maven-plugin.version>${frontend-maven-plugin.version}</frontend-maven-plugin.version>
    <maven-checkstyle-plugin.version>${maven-checkstyle-plugin.version}</maven-checkstyle-plugin.version>
    <maven-compiler-plugin.version>${maven-compiler-plugin.version}</maven-compiler-plugin.version>
    <maven.compiler.release>${maven.compiler.release}</maven.compiler.release>
    <node.lts.version>${node.lts.version}</node.lts.version>
    <checkstyle.version>${checkstyle.version}</checkstyle.version>
  </properties>
  ...
</project>
```

### 2. Add project build pluginManagement plugins

It is a good practice for leverage Maven inheritence features in all projects. If using a parent POM consider using 
`pluginManagement` to "pin" versions in projects "extending" the parent. If using a project build, `pluginManagement` 
helps project and profile builds too.

```xml
<project>
  ...
  <build>
    <pluginManagement>
      <plugins>
        <!-- other plugins MAY be present                           -->
        <plugin>
          <groupId>com.github.eirslett</groupId>
          <artifactId>frontend-maven-plugin</artifactId>
          <version>\${frontend-maven-plugin.version}</version>
          <configuration>
            <installDirectory>target</installDirectory>
          </configuration>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-checkstyle-plugin</artifactId>
          <version>\${maven-checkstyle-plugin.version}</version>
          <dependencies>
            <dependency>
              <groupId>com.puppycrawl.tools</groupId>
              <artifactId>checkstyle</artifactId>
              <version>\${checkstyle.version}</version>
            </dependency>
          </dependencies>
          <configuration>
            <!-- the checkstyle plugin can be configured here       -->
          </configuration>
          <executions>
            <execution>
              <id>validate</id>
              <phase>validate</phase>
              <goals>
                <goal>check</goal>
              </goals>
            </execution>
          </executions>
        </plugin>
        <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>\${maven-compiler-plugin.version}</version>
        </plugin>
      </plugins>
    </pluginManagement>
  </build>
  ...
</project>
```

See [frontend-maven-plugin](https://github.com/eirslett/frontend-maven-plugin) documentation for detailed configuration 
options.

### 3. Add dependencyManagement and dependencies

Like `pluginManagement`, `dependencyManagement` provides project level inheritence and is especially effective in parent
POMs.

The use of the dependency of Checkstyle here allows the `pre-commit` hook to load Checkstyle directly from the project 
classpath built by Maven.

```xml
<project>
  ...
  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.puppycrawl.tools</groupId>
        <artifactId>checkstyle</artifactId>
        <version>\${checkstyle.version}</version>
      </dependency>
    </dependencies>
  </dependencyManagement>
  ...
  ...
  <dependencies>
    <dependency>
      <groupId>com.puppycrawl.tools</groupId>
      <artifactId>checkstyle</artifactId>
      <!-- `version` is provided by `dependencyManagement`          -->
    </dependency>
  </dependencies>
  ...
</project>
```

### 4. Add a profile to bootstrap Husky

```xml
<project>
  ...
  <profiles>
    <profile>
      <id>husky</id>
      <build>
        <plugins>
        <!-- this is known as a "profile build"                     -->
        <plugin>
          <!-- `version` is provided by `pluginManagement`          -->
          <groupId>com.github.eirslett</groupId>
          <artifactId>frontend-maven-plugin</artifactId>
          <executions>
            <execution>
              <id>install-node-and-npm</id>
              <goals>
                <goal>install-node-and-npm</goal>
              </goals>
              <configuration>
                <nodeVersion>\${node.lts.version}</nodeVersion>
                <!-- npm will be extracted from the node distro     -->
              </configuration>
            </execution>
            <execution>
              <id>npm install</id>
              <goals>
                <goal>npm</goal>
              </goals>
            </execution>
            <execution>
              <id>husky install</id>
              <goals>
                <goal>npm</goal>
              </goals>
              <phase>generate-resources</phase>
              <configuration>
                <!-- a run script is provided in the package.json   -->
                <arguments>run husky:install</arguments>
              </configuration>
            </execution>
          </executions>
        </plugin>
        </plugins>
      </build>
    </profile>
  </profiles>
  ...
</project>
```

### 5. Commit

### 6. Run the husky profile

`mvn -Phusky install`

### 7. Test

Test a commit.

## commitlint

Commitlint enables [semantic versioning](https://semver.org) though release tooling such as the Maven Release Plugin.

Commitlint leverages the [Conventional Commits specification](https://www.conventionalcommits.org/en/v1.0.0/) to 
facilitate major, minor, and patch releases.

Let's look at the basics of the specification and what is expected of developers and tech leads.

**Basically, commit messages that don't meet the specification are rejected until corrected.**

### The Basic and Default Commit Format

<pre>
<a href="#types">&lt;type&gt;</a>(<a href="#scopes">&lt;optional scope&gt;</a>): <a href="#subject">&lt;subject&gt;</a>
empty separator line
<a href="#body">&lt;optional body&gt;</a>
empty separator line
<a href="#footer">&lt;optional footer&gt;</a>
</pre>

#### Types
* API relevant changes
    * `feat` commits add a new feature
    * `fix` commits fix a bug
* `docs` commits are documentation only
* `style` commits SHOULD not affect the meaning (white-space, formatting, missing semi-colons, etc.)
* `refactor` commits code changes that neither fixes a bug (`fix`) nor adds a feature (`feat`)
* `perf` commits improve performance
* `test` commits add missing tests or correcting existing tests
* `build` commits affect the build system or external dependencies
* `ci` commits CI configuration files and scripts
* `chore` commits include changes that don't modify `src` or test file, e.g., `.gitignore` changes
* `revert` commit, well, revert a previous commit. 

**Only** `feat` and `fix` generate a release, minor and patch, respectively. A [`footer`](#footer) will generate a major
release, *only* if containing the defined markers of Conventional Commits, i.e., "BREAKING CHANGE:".

#### Scopes
The `scope` provides additional contextual information.
* Don't use issue identifiers as scopes
* Is an **optional** part of the format
* Allowed Scopes depends on the specific project

See [Defining Scopes](#defining-scopes) for more information about enumerating scopes in a project.

#### Subject
The `subject` contains a succinct description of the change.
* Is a **mandatory** part of the format
* Use the imperative, present tense: "change" not "changed" nor "changes"
* Don't capitalize the first letter
* No dot (.) at the end

#### Body
The `body` SHOULD include the motivation for the change and contrast this with previous behavior.
* Is an **optional** part of the format
* Use the imperative, present tense: "change" not "changed" nor "changes"
* This is the place to mention issue identifiers and their relations

#### Footer
The `footer` SHOULD contain any information about **Breaking Changes** and is also the place to **reference Issues** 
that this commit refers to.
* Is an **optional** part of the format
* **optionally** reference an issue by its id.
* **Breaking Changes** SHOULD start with the word `BREAKING CHANGES:` followed by space or two newlines. The rest of the
commit message is then used for in generating a major release.

# Advanced Usage

## Checkstyle

[checkstyle.org](https://checkstyle.org) notes the following:

> Checkstyle is a development tool to help programmers write Java code that adheres to a coding standard. It automates 
> the process of checking Java code to spare humans of this boring (but important) task. This makes it ideal for 
> projects that want to enforce a coding standard.

### Implementation notes

This packaging uses Checkstyle 10+. This version of checkstyle MUST be executed in a Java 11+ development environment. 
As Java 11 is "the new 8" this MAY be a blocker for the target project.

This is why you see the configuration requirements above.

### Build

The installation instructions include defining the Checkstyle plugin as part of the build. This will enforce the same rules 
used by the githooks in the build pipeline. If the configuration defines any errors, the build will fail.

### Check file

The `pre-commit` hook uses the Google checkfile, `google_checks.xml`, provided by Checkstyle. The specific coverage of that file 
is found here, [Google's Java Style Checkstyle Coverage](https://checkstyle.org/google_style.html)

#### Custom Checkfile

Creating a custom checkfile is beyond the scope of this project, but is [detailed at checkstyle.org](https://checkstyle.org/config.html).
It is often easier to copy an existing checkfile and edit it. 

Loading a custom checkfile is straight forward: create a JAR and load it as a dependency to the project. For example:

```text

user-checks
  |
  + src/main/resources
  |            |
  |            | user-checks.xml
  |
  | pom.xml
```

This proposed Maven JAR project, "com.corp.user-checks," can be deployed to Maven Central or an internal mirror for distribution. 

Add the JAR as a dependency, for example:

```xml
<dependency>
  <groupId>com.corp</groupId>
  <artifactId>user-checks</artifactId>
  <version>1.0.0</version
</dependency>
```

In the project pipeline, e.g., GitLab, GitHub, &tc., set a environment variable of `CHECKFILE`. This will override the default,
`google-checks.xml`.

This has some additional benefits to the organization, as it makes checks centrally distributed and less likely to be 
"overriden" by project teams to avoid the "censure" of Checkstyle.

## Defining commitlint Scopes

By default `commitlint` does not define any scopes, thus scopes can be _ad hoc_ in a project.

To define scopes, simply add scopes to the internal array of the `scope-enum` array, provided empty. An example follows:

```javascript

  'rules': {
    'scope-enum': [1, 'always', [ 'checkstyle', 'config', 'gitlab', 'husky', 'maven', 'package', 'ossrh' ]],
  },
```

### What scopes?

Scopes are  _ad hoc_ and the distribution comes with the above pre-defined. Users are encouraged to define their own through
use and updates. Two RECOMMENDATIONS follows:

1. Avoid "pairing," e.g., `docs(docs)`. Instead `docs(javadocs)` or `docs(readme)` are RECOMMENDED, where `javadoc` and `readme` 
are defined in the `scope-enum`. 
1. Allow scopes to be defined for a period and then update the `scope-enum` from the commits by the team. The `scope-enum` is 
set to only _warn_ on commit (`1`) so, out-of-the-box, having an undefined scope does not prevent commits.

The list of available commitlint rules are here, [commitlint reference](https://commitlint.js.org/#/reference-rules?id=available-rules).

#### commitlint-plugin-function-rules

Support for commitlint's plugin function rules is provided allowing for scope checking to regular expression. This is very effective for
allowing scopes to match JIRA issues (as well as other issue tracker formats).

## Installation for non-Java Project
 
A project using another language platform, e.g., Node.js, Ruby, others, can get some of the benefits of Commit Controls, especially 
the Conventional Commits and Git githooks integration provided by Husky.

[Extract the release zip](#0-extract-the-release-zip) as before.

:warning: A `package.json` may already be present in the target "non-Java" project. When unpacking the release zip, choose one of the following
from the unarchiver if presented options:

1. Rename `package.json` to `cc-package.json` (recommended, if presented)
2. Overwrite, Move and Restore
    - Overwrite the `package.json`
    - Move the overwritten file to another name, e.g., `cc-package.json`
    - Restore the previous file, e.g., `git restore package.json` (or other source control restore method)

### Update existing package.json

Add the following items to `devDependencies` (check the commas!). If no `devDependencies` "object" see [direct NPM install below](#direct-npm-install-option).

```json
"@commitlint/cli": "^17.0.3",
"@commitlint/config-conventional": "^17.0.3",
"husky": "^8.0.1"
```

#### Direct NPM Install Option

The needed development dependencies can also be installed directly with NPM as follows:

```shell
npm install --save-dev @commitlint/cli@17.0.3
npm install --save-dev @commitlint/config-conventional@17.0.3
npm install --save-dev husky@8.0.1
```

### Remove or Edit the pre-commit hook

Commit Controls includes a `pre-commit` hook in the `.husky` folder. The current `pre-commit` hook is for Java Checkstyles.

This MAY be deleted or edited for the needs of the project.
