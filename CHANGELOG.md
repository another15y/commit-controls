## [1.1.2](https://gitlab.com/another15y/commit-controls/compare/1.1.1...1.1.2) (2023-07-17)


### Bug Fixes

* update plugins and dependencies to current versions ([7fdd9cb](https://gitlab.com/another15y/commit-controls/commit/7fdd9cbebb9a821677fd38c681dd362a4a9b77b7))

# Another Caffeinated Day Commit Controls


# [1.1.0](https://gitlab.com/another15y/commit-controls/compare/1.0.7...1.1.0) (2023-07-17)


### Features

* add is-ci guard for husky install ([6ef58be](https://gitlab.com/another15y/commit-controls/commit/6ef58bef5c2b721a8b59198552c368bb81c5bcc7))
