module.exports = {
  'extends': ['@commitlint/config-conventional'],
  'plugins': ['commitlint-plugin-function-rules'],
  'rules': {
    'scope-enum': [0],
    'function-rules/scope-enum': [1, 'always', (parsed) => {
        const scopes = [ 'README', 'checkstyle', 'config', 'gitlab', 'husky', 'maven', 'package', 'ossrh' ];
        if (!parsed.scope || scopes.some( (scope) => new RegExp('^' + scope).test(parsed.scope) )) {
          return [true];
        }
        return [false, 'scope SHOULD match (by regulare expression) one of\n    ' + scopes.join(', ')];
      }
    ]
  }
};
